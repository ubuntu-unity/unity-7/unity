# Japanese translations for l package.
# Copyright (C) 2010 THE l'S COPYRIGHT HOLDER
# This file is distributed under the same license as the l package.
# Canonical OEM, 2010.
# 
msgid ""
msgstr ""
"Project-Id-Version: l 10n\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2022-04-29 15:33+0530\n"
"PO-Revision-Date: 2010-03-02 12:36-0500\n"
"Last-Translator: Canonical OEM\n"
"Language-Team: Japanese\n"
"Language: \n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=1; plural=0;\n"

#: ../shortcuts/CompizShortcutModeller.cpp:128
#: ../shortcuts/CompizShortcutModeller.cpp:231
msgid " (Hold)"
msgstr ""

#: ../shortcuts/CompizShortcutModeller.cpp:169
#: ../shortcuts/CompizShortcutModeller.cpp:225
msgid " (Tap)"
msgstr ""

#: ../shortcuts/CompizShortcutModeller.cpp:146
msgid " + 1 to 9"
msgstr ""

#: ../shortcuts/CompizShortcutModeller.cpp:325
#: ../shortcuts/CompizShortcutModeller.cpp:331
msgid " + Arrow Keys"
msgstr ""

#: ../shortcuts/CompizShortcutModeller.cpp:152
msgid " + Shift + 1 to 9"
msgstr ""

#: ../shortcuts/CompizShortcutModeller.cpp:417
#: ../shortcuts/CompizShortcutModeller.cpp:423
msgid " Drag"
msgstr ""

#: ../shortcuts/CompizShortcutModeller.cpp:394
msgid " or Right"
msgstr ""

#: ../panel/PanelMenuView.cpp:82
#, c-format
msgid "%s Desktop"
msgstr ""

#. The "%s" is used in the dash preview to display the "<hint>: <value>" infos
#: ../dash/previews/PreviewInfoHintWidget.cpp:168
#, c-format
msgid "%s:"
msgstr ""

#: ../a11y/unity-launcher-icon-accessible.cpp:303
#, c-format
msgid "%s: running"
msgstr ""

#: ../a11y/unity-launcher-icon-accessible.cpp:299
#, c-format
msgid "%s: running: %zu windows open"
msgstr ""

#: ../a11y/unity-scope-bar-icon-accessible.cpp:164
#, c-format
msgid "%s: selected"
msgstr ""

#: ../plugins/unityshell/unityshell.xml.in.h:87
msgid ""
"A conglomerate setting that modifies the overall responsiveness of the "
"Launcher reveal."
msgstr ""

#: ../plugins/unityshell/unityshell.xml.in.h:9
msgid "A tap on this key summons the HUD."
msgstr ""

#: ../services/panel-service.c:2395
msgid "Activate"
msgstr ""

#: ../plugins/unityshell/unityshell.xml.in.h:30
msgid "Active Blur"
msgstr ""

#: ../plugins/unityshell/unityshell.xml.in.h:44
msgid "Active window shadow color"
msgstr ""

#: ../plugins/unityshell/unityshell.xml.in.h:42
msgid "Active window shadow radius"
msgstr ""

#: ../launcher/ApplicationLauncherIcon.cpp:723
msgid "Add to Dash"
msgstr ""

#: ../dash/FilterAllButton.cpp:38
msgid "All"
msgstr ""

#: ../plugins/unityshell/unityshell.xml.in.h:78
msgid "All Displays"
msgstr ""

#: ../plugins/unityshell/unityshell.xml.in.h:85
msgid ""
"Allows minimizing a single windowed application by clicking on its Launcher "
"icon."
msgstr ""

#: ../plugins/unityshell/unityshell.xml.in.h:83
msgid ""
"Allows using the mouse scrollwheel to focus an application if the icon is "
"inactive."
msgstr ""

#: ../plugins/unityshell/unityshell.xml.in.h:91
msgid ""
"Amount of mouse pressure required to push the mousepointer into the next "
"monitor."
msgstr ""

#: ../plugins/unityshell/unityshell.xml.in.h:89
msgid "Amount of mouse pressure required to reveal the Launcher."
msgstr ""

#: ../plugins/unityshell/unityshell.xml.in.h:68
msgid "Animation played when the Launcher is showing or hiding."
msgstr ""

#: ../shortcuts/CompizShortcutModeller.cpp:213
msgid "Arrow Keys"
msgstr ""

#: ../lockscreen/UserPromptView.cpp:508
msgid "Authentication failure"
msgstr ""

#: ../plugins/unityshell/unityshell.xml.in.h:66
msgid "Autohide"
msgstr ""

#: ../plugins/unityshell/unityshell.xml.in.h:145
msgid ""
"Automatically spreads multiple windows of the same application out into a "
"grid after a short time."
msgstr ""

#: ../plugins/unityshell/unityshell.xml.in.h:31
msgid "Automaximize Value"
msgstr ""

#: ../plugins/unityshell/unityshell.xml.in.h:24
msgid "Background Color"
msgstr ""

#: ../plugins/unityshell/unityshell.xml.in.h:25
msgid "Background color override for the Dash, Launcher and Switcher."
msgstr ""

#: ../plugins/unityshell/unityshell.xml.in.h:104
msgid "Backlight Always Off"
msgstr ""

#: ../plugins/unityshell/unityshell.xml.in.h:102
msgid "Backlight Always On"
msgstr ""

#: ../plugins/unityshell/unityshell.xml.in.h:103
msgid "Backlight Toggles"
msgstr ""

#: ../plugins/unityshell/unityshell.xml.in.h:106
msgid "Backlight and Edge Illumination Toggles"
msgstr ""

#: ../plugins/unityshell/unityshell.xml.in.h:146
msgid "Bias alt-tab to prefer windows on the current viewport"
msgstr ""

#: ../plugins/unityshell/unityshell.xml.in.h:147
msgid ""
"Bias the Switcher to prefer windows which are placed on the current viewport."
msgstr ""

#: ../plugins/unityshell/unityshell.xml.in.h:111
msgid "Blink"
msgstr ""

#: ../unity-shared/TextInput.cpp:347
msgid "Caps lock is on"
msgstr ""

#: ../dash/FilterGenreWidget.cpp:47
msgid "Categories"
msgstr ""

#: ../plugins/unityshell/unityshell.xml.in.h:101
msgid "Change how the icons in the Launcher are backlit."
msgstr ""

#: ../launcher/ApplicationLauncherIcon.cpp:661
msgid "Close"
msgstr ""

#: ../shortcuts/CompizShortcutModeller.cpp:401
msgid "Closes the current window."
msgstr ""

#: ../shortcuts/CompizShortcutModeller.cpp:308
msgid "Closes the selected application / window."
msgstr ""

#: ../dash/previews/SocialPreview.cpp:230
msgid "Comments"
msgstr ""

#: ../shortcuts/CompizShortcutModeller.cpp:415
msgid "Ctrl + Alt + Num (keypad)"
msgstr ""

#: ../shortcuts/CompizShortcutModeller.cpp:208
msgid "Ctrl + Tab"
msgstr ""

#: ../shortcuts/CompizShortcutModeller.cpp:246
#: ../shortcuts/CompizShortcutModeller.cpp:288
msgid "Cursor Left or Right"
msgstr ""

#: ../shortcuts/CompizShortcutModeller.cpp:293
msgid "Cursor Up or Down"
msgstr ""

#: ../plugins/unityshell/unityshell.xml.in.h:58
msgid ""
"Cycles through icons present in the Launcher, in reverse order. Activates "
"the highlighted icon on release."
msgstr ""

#: ../plugins/unityshell/unityshell.xml.in.h:56
msgid ""
"Cycles through icons present in the Launcher. Activates the highlighted icon "
"on release."
msgstr ""

#: ../a11y/unity-dash-view-accessible.cpp:100
#: ../shortcuts/CompizShortcutModeller.cpp:167
msgid "Dash"
msgstr ""

#: ../plugins/unityshell/unityshell.xml.in.h:26
msgid "Dash Blur"
msgstr ""

#: ../plugins/unityshell/unityshell.xml.in.h:59
msgid "Dash tap duration"
msgstr ""

#: ../plugins/unityshell/unityshell.xml.in.h:35
msgid "Decorations"
msgstr ""

#: ../plugins/unityshell/unityshell.xml.in.h:81
msgid "Determines if the Launcher's edges will capture the mousepointer."
msgstr ""

#: ../plugins/unityshell/unityshell.xml.in.h:148
msgid "Disable Show Desktop in the Switcher"
msgstr ""

#: ../plugins/unityshell/unityshell.xml.in.h:150
msgid "Disable the mouse in the Switcher"
msgstr ""

#: ../plugins/unityshell/unityshell.xml.in.h:149
msgid "Disables the Show Desktop icon in the Switcher."
msgstr ""

#: ../plugins/unityshell/unityshell.xml.in.h:141
msgid ""
"Disables the last focused window being the first entry in the Switcher, so "
"Switcher strictly switches between applications"
msgstr ""

#: ../plugins/unityshell/unityshell.xml.in.h:151
msgid "Disables the mouse in the Switcher."
msgstr ""

#: ../decorations/DecorationsForceQuitDialog.cpp:338
msgid ""
"Do you want to force the application to exit, or wait for it to respond?"
msgstr ""

#: ../launcher/SpacerLauncherIcon.cpp:35
msgid "Drop To Add Application"
msgstr ""

#: ../plugins/unityshell/unityshell.xml.in.h:124
msgid ""
"Duration (in milliseconds) of the menus fade-in animation, used when the "
"menus of a new launched application have been shown."
msgstr ""

#: ../plugins/unityshell/unityshell.xml.in.h:118
msgid ""
"Duration (in milliseconds) of the menus fade-in animation, used when the "
"mouse goes over the top-panel."
msgstr ""

#: ../plugins/unityshell/unityshell.xml.in.h:126
msgid ""
"Duration (in milliseconds) of the menus fade-out animation, used when the "
"menus of a new launched application have been shown."
msgstr ""

#: ../plugins/unityshell/unityshell.xml.in.h:120
msgid ""
"Duration (in milliseconds) of the menus fade-out animation, used when the "
"mouse goes over the top-panel."
msgstr ""

#: ../plugins/unityshell/unityshell.xml.in.h:60
msgid ""
"Duration (in millseconds) that will count as a tap for opening the Dash."
msgstr ""

#: ../plugins/unityshell/unityshell.xml.in.h:96
msgid "Duration of Sticky Edge Release after Break"
msgstr ""

#: ../plugins/unityshell/unityshell.xml.in.h:37
msgid ""
"Each Gtk theme can provide shadow parameters, but you can override these "
"values here."
msgstr ""

#: ../plugins/unityshell/unityshell.xml.in.h:105
msgid "Edge Illumination Toggles"
msgstr ""

#: ../plugins/unityshell/unityshell.xml.in.h:94
msgid "Edge Stop Velocity"
msgstr ""

#: ../launcher/VolumeLauncherIcon.cpp:272
msgid "Eject"
msgstr ""

#: ../launcher/VolumeLauncherIcon.cpp:272
msgid "Eject parent drive"
msgstr ""

#: ../launcher/TrashLauncherIcon.cpp:103
msgid "Empty Trash…"
msgstr ""

#: ../plugins/unityshell/unityshell.xml.in.h:33
msgid "Enable Shortcut Hints Overlay"
msgstr ""

#: ../plugins/unityshell/unityshell.xml.in.h:143
msgid "Enables miniature live window previews in the Switcher."
msgstr ""

#: ../plugins/unityshell/unityshell.xml.in.h:34
msgid ""
"Enables possibility to display an overlay showing available mouse and "
"keyboard shortcuts."
msgstr ""

#: ../shortcuts/CompizShortcutModeller.cpp:218
msgid "Enter"
msgstr ""

#: ../shortcuts/CompizShortcutModeller.cpp:291
msgid "Enter / Exit from spread mode or Select windows."
msgstr ""

#: ../launcher/ApplicationLauncherIcon.cpp:660
msgid "Exit"
msgstr ""

#: ../plugins/unity-mt-grab-handles/unitymtgrabhandles.xml.in.h:9
msgid "Fade Duration"
msgstr ""

#: ../plugins/unityshell/unityshell.xml.in.h:72
msgid "Fade and Slide"
msgstr ""

#: ../plugins/unityshell/unityshell.xml.in.h:69
msgid "Fade on bfb and Slide"
msgstr ""

#: ../plugins/unityshell/unityshell.xml.in.h:71
msgid "Fade only"
msgstr ""

#: ../lockscreen/UserPromptView.cpp:301
msgid "Failed to authenticate"
msgstr ""

#: ../unity-shared/SearchBar.cpp:163
msgid "Filter results"
msgstr ""

#: ../plugins/unityshell/unityshell.xml.in.h:139
msgid ""
"Flips through all the windows present in the Switcher, in reverse order."
msgstr ""

#: ../plugins/unityshell/unityshell.xml.in.h:137
msgid "Flips through all the windows present in the Switcher."
msgstr ""

#: ../launcher/VolumeLauncherIcon.cpp:312
msgid "Format…"
msgstr ""

#: ../plugins/unityshell/unityshell.xml.in.h:3
msgid "General"
msgstr ""

#: ../plugins/unityshell/unityshell.xml.in.h:54
msgid ""
"Gives keyboard-focus to the Launcher so it can be navigated with the cursor-"
"keys."
msgstr ""

#: ../shutdown/SessionView.cpp:175
#, c-format
msgid ""
"Goodbye, %s. Are you sure you want to close all programs and log out from "
"your account?"
msgstr ""

#: ../shutdown/SessionView.cpp:159
#, c-format
msgid ""
"Goodbye, %s. Are you sure you want to close all programs and shut down the "
"computer?"
msgstr ""

#: ../shutdown/SessionView.cpp:204
#, c-format
msgid "Goodbye, %s. Would you like to…"
msgstr ""

#: ../launcher/HudLauncherIcon.cpp:44
msgid "HUD"
msgstr ""

#: ../shortcuts/CompizShortcutModeller.cpp:223
msgid "HUD & Menu Bar"
msgstr ""

#: ../shutdown/SessionView.cpp:170
#, c-format
msgid ""
"Hi %s, you have open files that you might want to save before logging out. "
"Are you sure you want to continue?"
msgstr ""

#: ../shutdown/SessionView.cpp:154
#, c-format
msgid ""
"Hi %s, you have open files that you might want to save before shutting down. "
"Are you sure you want to continue?"
msgstr ""

#: ../shutdown/SessionView.cpp:198
#, c-format
msgid ""
"Hi %s, you have open files you might want to save.\n"
"Would you like to…"
msgstr ""

#. We have enough buttons to show the message without a new line.
#: ../shutdown/SessionView.cpp:193
#, c-format
msgid "Hi %s, you have open files you might want to save. Would you like to…"
msgstr ""

#: ../shutdown/SessionButton.cpp:71
msgid "Hibernate"
msgstr ""

#: ../plugins/unityshell/unityshell.xml.in.h:67
msgid "Hide Animation"
msgstr ""

#: ../plugins/unity-mt-grab-handles/unitymtgrabhandles.xml.in.h:7
#, fuzzy
msgid "Hide Handles"
msgstr "Launcher から削除"

#: ../plugins/unityshell/unityshell.xml.in.h:63
#, fuzzy
msgid "Hide Launcher"
msgstr "Launcher から削除"

#: ../plugins/unityshell/unityshell.xml.in.h:13
msgid "Hide all windows and focus desktop."
msgstr ""

#: ../plugins/unityshell/unityshell.xml.in.h:52
msgid ""
"Holding this key makes the Launcher and Help Overlay appear. Tapping it "
"opens the Dash."
msgstr ""

#: ../plugins/unityshell/unityshell.xml.in.h:39
msgid "Horizontal offset of the shadow."
msgstr ""

#: ../plugins/unity-mt-grab-handles/unitymtgrabhandles.xml.in.h:10
msgid "How long the fade should last"
msgstr ""

#: ../plugins/unityshell/unityshell.xml.in.h:122
msgid ""
"How many seconds the menus should be shown when a new application has been "
"launched."
msgstr ""

#: ../plugins/unityshell/unityshell.xml.in.h:48
msgid "Inactive windows shadow color"
msgstr ""

#: ../plugins/unityshell/unityshell.xml.in.h:46
msgid "Inactive windows shadow radius"
msgstr ""

#: ../plugins/unityshell/unityshell.xml.in.h:19
msgid ""
"Initiate the window Spread for the current application windows, for all the "
"windows."
msgstr ""

#: ../plugins/unityshell/unityshell.xml.in.h:17
msgid "Initiate the window Spread for the current application windows."
msgstr ""

#: ../launcher/SoftwareCenterLauncherIcon.cpp:153
msgid "Installing…"
msgstr ""

#: ../lockscreen/UserPromptView.cpp:296
msgid "Invalid password, please try again"
msgstr ""

#: ../plugins/unityshell/unityshell.xml.in.h:10
msgid "Key to execute a command"
msgstr ""

#: ../plugins/unityshell/unityshell.xml.in.h:136
msgid "Key to flip through windows in the Switcher"
msgstr ""

#: ../plugins/unityshell/unityshell.xml.in.h:138
msgid "Key to flip through windows in the Switcher backwards"
msgstr ""

#: ../plugins/unityshell/unityshell.xml.in.h:53
msgid "Key to give keyboard-focus to the Launcher"
msgstr ""

#: ../plugins/unity-mt-grab-handles/unitymtgrabhandles.xml.in.h:8
msgid "Key to hide the handles"
msgstr ""

#: ../plugins/unityshell/unityshell.xml.in.h:6
msgid "Key to lock the screen."
msgstr ""

#: ../plugins/unityshell/unityshell.xml.in.h:14
msgid "Key to open the first panel menu"
msgstr ""

#: ../plugins/unityshell/unityshell.xml.in.h:51
msgid "Key to show the Dash, Launcher and Help Overlay"
msgstr ""

#: ../gnome/50-unity-launchers.xml.in.h:2
msgid "Key to show the HUD"
msgstr ""

#: ../plugins/unityshell/unityshell.xml.in.h:8
msgid "Key to show the HUD when tapped"
msgstr ""

#: ../plugins/unity-mt-grab-handles/unitymtgrabhandles.xml.in.h:6
msgid "Key to show the handles"
msgstr ""

#: ../plugins/unityshell/unityshell.xml.in.h:4
msgid "Key to show the menu bar while pressed"
msgstr ""

#: ../plugins/unityshell/unityshell.xml.in.h:16
msgid "Key to spread the current application windows"
msgstr ""

#: ../plugins/unityshell/unityshell.xml.in.h:18
msgid "Key to spread the current application windows in any workspace"
msgstr ""

#: ../plugins/unityshell/unityshell.xml.in.h:55
msgid "Key to start the Launcher Application Switcher"
msgstr ""

#: ../plugins/unityshell/unityshell.xml.in.h:57
msgid "Key to start the Launcher Application Switcher in reverse"
msgstr ""

#: ../plugins/unityshell/unityshell.xml.in.h:128
msgid "Key to start the Switcher"
msgstr ""

#: ../plugins/unityshell/unityshell.xml.in.h:132
msgid "Key to start the Switcher for all viewports"
msgstr ""

#: ../plugins/unityshell/unityshell.xml.in.h:130
msgid "Key to switch to the previous window in the Switcher"
msgstr ""

#: ../plugins/unityshell/unityshell.xml.in.h:134
msgid "Key to switch to the previous window in the Switcher for all viewports"
msgstr ""

#: ../plugins/unity-mt-grab-handles/unitymtgrabhandles.xml.in.h:4
msgid "Key to toggle the handles"
msgstr ""

#: ../shortcuts/ShortcutView.cpp:85
msgid "Keyboard Shortcuts"
msgstr ""

#: ../a11y/unity-launcher-accessible.cpp:137
#: ../plugins/unityshell/unityshell.xml.in.h:50
#: ../shortcuts/CompizShortcutModeller.cpp:126
msgid "Launcher"
msgstr ""

#: ../plugins/unityshell/unityshell.xml.in.h:80
msgid "Launcher Capture Mouse"
msgstr ""

#: ../plugins/unityshell/unityshell.xml.in.h:90
msgid "Launcher Edge Stop Overcome Pressure"
msgstr ""

#: ../plugins/unityshell/unityshell.xml.in.h:100
msgid "Launcher Icon Backlight Mode"
msgstr ""

#: ../plugins/unityshell/unityshell.xml.in.h:107
msgid "Launcher Icon Launch Animation"
msgstr ""

#: ../plugins/unityshell/unityshell.xml.in.h:98
msgid "Launcher Icon Size"
msgstr ""

#: ../plugins/unityshell/unityshell.xml.in.h:112
msgid "Launcher Icon Urgent Animation"
msgstr ""

#: ../plugins/unityshell/unityshell.xml.in.h:76
#, fuzzy
msgid "Launcher Monitors"
msgstr "Launcher から削除"

#: ../plugins/unityshell/unityshell.xml.in.h:61
#, fuzzy
msgid "Launcher Opacity"
msgstr "Launcher から削除"

#: ../plugins/unityshell/unityshell.xml.in.h:86
msgid "Launcher Reveal Edge Responsiveness"
msgstr ""

#: ../plugins/unityshell/unityshell.xml.in.h:88
msgid "Launcher Reveal Pressure"
msgstr ""

#. Namespace
#: ../gnome/50-unity-launchers.xml.in.h:1
#, fuzzy
msgid "Launchers"
msgstr "Launcher から削除"

#: ../plugins/unityshell/unityshell.xml.in.h:74
msgid "Left Edge"
msgstr ""

#: ../shortcuts/ShortcutHintPrivate.cpp:84
msgid "Left Mouse"
msgstr ""

#: ../shutdown/SessionButton.cpp:59
msgid "Lock"
msgstr ""

#: ../launcher/ApplicationLauncherIcon.cpp:590
#: ../launcher/ApplicationLauncherIcon.cpp:713
#: ../launcher/VolumeLauncherIcon.cpp:189
#, fuzzy
msgid "Lock to Launcher"
msgstr "Launcher から削除"

#: ../shutdown/SessionView.cpp:165 ../shutdown/SessionButton.cpp:63
msgid "Log Out"
msgstr ""

#: ../plugins/unityshell/unityshell.xml.in.h:64
msgid "Make the Launcher hide automatically after some time of inactivity."
msgstr ""

#: ../shortcuts/CompizShortcutModeller.cpp:383
msgid "Maximises the current window."
msgstr ""

#: ../plugins/unityshell/unityshell.xml.in.h:116
msgid "Menus"
msgstr ""

#: ../plugins/unityshell/unityshell.xml.in.h:121
msgid "Menus Discovery Duration"
msgstr ""

#: ../plugins/unityshell/unityshell.xml.in.h:123
msgid "Menus Discovery Fade-in Duration"
msgstr ""

#: ../plugins/unityshell/unityshell.xml.in.h:125
msgid "Menus Discovery Fade-out Duration"
msgstr ""

#: ../plugins/unityshell/unityshell.xml.in.h:117
msgid "Menus Fade-in Duration"
msgstr ""

#: ../plugins/unityshell/unityshell.xml.in.h:119
msgid "Menus Fade-out Duration"
msgstr ""

#: ../shortcuts/ShortcutHintPrivate.cpp:85
msgid "Middle Mouse"
msgstr ""

#: ../shortcuts/CompizShortcutModeller.cpp:377
msgid "Minimises all windows."
msgstr ""

#: ../plugins/unityshell/unityshell.xml.in.h:84
msgid "Minimize Single Window Applications (Unsupported)"
msgstr ""

#: ../shortcuts/CompizShortcutModeller.cpp:244
msgid "Moves focus between indicators."
msgstr ""

#: ../shortcuts/CompizShortcutModeller.cpp:332
msgid "Moves focused window to another workspace."
msgstr ""

#: ../shortcuts/CompizShortcutModeller.cpp:211
#: ../shortcuts/CompizShortcutModeller.cpp:286
msgid "Moves the focus."
msgstr ""

#: ../shortcuts/CompizShortcutModeller.cpp:418
msgid "Moves the window."
msgstr ""

#: ../dash/FilterMultiRangeWidget.cpp:43
msgid "Multi-range"
msgstr ""

#: ../plugins/unityshell/unityshell.xml.in.h:65
msgid "Never"
msgstr ""

#: ../plugins/unityshell/unityshell.xml.in.h:28
msgid "No Blur"
msgstr ""

#: ../unity-shared/CoverArt.cpp:452
msgid "No Image Available"
msgstr ""

#: ../plugins/unityshell/unityshell.xml.in.h:109
msgid "None"
msgstr ""

#: ../launcher/VolumeLauncherIcon.cpp:254
msgid "Open"
msgstr ""

#: ../shortcuts/CompizShortcutModeller.cpp:135
msgid "Opens Launcher keyboard navigation mode."
msgstr ""

#: ../plugins/unityshell/unityshell.xml.in.h:11
msgid "Opens a folder or executes a command."
msgstr ""

#: ../shortcuts/CompizShortcutModeller.cpp:153
msgid "Opens a new window in the app."
msgstr ""

#: ../shortcuts/CompizShortcutModeller.cpp:176
msgid "Opens the Dash App Lens."
msgstr ""

#: ../shortcuts/CompizShortcutModeller.cpp:182
msgid "Opens the Dash Files Lens."
msgstr ""

#: ../shortcuts/CompizShortcutModeller.cpp:170
msgid "Opens the Dash Home."
msgstr ""

#: ../shortcuts/CompizShortcutModeller.cpp:188
msgid "Opens the Dash Music Lens."
msgstr ""

#: ../shortcuts/CompizShortcutModeller.cpp:194
msgid "Opens the Dash Photo Lens."
msgstr ""

#: ../shortcuts/CompizShortcutModeller.cpp:200
msgid "Opens the Dash Video Lens."
msgstr ""

#: ../shortcuts/CompizShortcutModeller.cpp:226
msgid "Opens the HUD."
msgstr ""

#: ../shortcuts/CompizShortcutModeller.cpp:129
msgid "Opens the Launcher, displays shortcuts."
msgstr ""

#: ../shortcuts/CompizShortcutModeller.cpp:159
msgid "Opens the Trash."
msgstr ""

#: ../shortcuts/CompizShortcutModeller.cpp:216
msgid "Opens the currently focused item."
msgstr ""

#: ../plugins/unityshell/unityshell.xml.in.h:15
msgid ""
"Opens the first indicator menu of the Panel, allowing keyboard navigation "
"thereafter."
msgstr ""

#: ../shortcuts/CompizShortcutModeller.cpp:238
msgid "Opens the indicator menu."
msgstr ""

#: ../shortcuts/CompizShortcutModeller.cpp:407
msgid "Opens the window accessibility menu."
msgstr ""

#: ../shutdown/SessionView.cpp:140
msgid ""
"Other users are logged in. Restarting or shutting down will close their open "
"applications and may cause them to lose work.\n"
"\n"
msgstr ""

#: ../plugins/unityshell/unityshell.xml.in.h:36
msgid "Override Theme Settings"
msgstr ""

#: ../plugins/unityshell/unityshell.xml.in.h:20
msgid "Panel Opacity"
msgstr ""

#: ../plugins/unityshell/unityshell.xml.in.h:22
msgid "Panel Opacity for Maximized Windows Toggle"
msgstr ""

#: ../lockscreen/UserPromptView.cpp:98
msgid "Password"
msgstr ""

#: ../shortcuts/CompizShortcutModeller.cpp:413
msgid "Places the window in corresponding position."
msgstr ""

#: ../plugins/unityshell/unityshell.xml.in.h:2
msgid "Plugin to draw the Unity Shell"
msgstr ""

#: ../plugins/unityshell/unityshell.xml.in.h:7
msgid "Pressing this key will lock the current session."
msgstr ""

#: ../plugins/unityshell/unityshell.xml.in.h:92
msgid "Pressure Decay Rate"
msgstr ""

#: ../plugins/unityshell/unityshell.xml.in.h:79
msgid "Primary Display"
msgstr ""

#: ../plugins/unityshell/unityshell.xml.in.h:114
msgid "Pulse"
msgstr ""

#: ../plugins/unityshell/unityshell.xml.in.h:110
msgid "Pulse Until Running"
msgstr ""

#: ../a11y/unity-quicklist-menu-accessible.cpp:179
msgid "Quicklist"
msgstr ""

#: ../launcher/ApplicationLauncherIcon.cpp:604
#: ../launcher/ApplicationLauncherIcon.cpp:659
#: ../launcher/ApplicationLauncherIcon.cpp:738
#: ../launcher/TrashLauncherIcon.cpp:130 ../launcher/VolumeLauncherIcon.cpp:376
msgid "Quit"
msgstr "中止"

#: ../dash/FilterRatingsWidget.cpp:48
msgid "Rating"
msgstr ""

#: ../shortcuts/CompizShortcutModeller.cpp:424
msgid "Resizes the window."
msgstr ""

#: ../shutdown/SessionButton.cpp:79
msgid "Restart"
msgstr ""

#: ../launcher/DesktopLauncherIcon.cpp:55
msgid "Restore Windows"
msgstr ""

#: ../shortcuts/CompizShortcutModeller.cpp:389
msgid "Restores or minimises the current window."
msgstr ""

#: ../lockscreen/UserPromptView.cpp:471
msgid "Retry"
msgstr ""

#: ../plugins/unityshell/unityshell.xml.in.h:73
msgid "Reveal Trigger"
msgstr ""

#: ../shortcuts/CompizShortcutModeller.cpp:232
msgid "Reveals the application menu."
msgstr ""

#: ../plugins/unityshell/unityshell.xml.in.h:5
msgid "Reveals the global menu bar while pressed."
msgstr ""

#: ../shortcuts/ShortcutHintPrivate.cpp:86
msgid "Right Mouse"
msgstr ""

#: ../launcher/VolumeLauncherIcon.cpp:291
msgid "Safely remove"
msgstr ""

#: ../launcher/VolumeLauncherIcon.cpp:291
msgid "Safely remove parent drive"
msgstr ""

#: ../shortcuts/CompizShortcutModeller.cpp:147
msgid "Same as clicking on a Launcher icon."
msgstr ""

#: ../plugins/unityshell/unityshell.xml.in.h:82
msgid "Scroll Inactive Icon to Focus Application"
msgstr ""

#: ../launcher/BFBLauncherIcon.cpp:148
msgid "Search your computer"
msgstr ""

#: ../launcher/BFBLauncherIcon.cpp:147
msgid "Search your computer and online sources"
msgstr ""

#: ../dash/PlacesGroup.cpp:386
msgid "See fewer results"
msgstr ""

#: ../dash/PlacesGroup.cpp:392
#, c-format
msgid "See one more result"
msgid_plural "See %d more results"
msgstr[0] ""

#: ../plugins/unityshell/unityshell.xml.in.h:77
msgid "Selects on which display the Launcher will be present."
msgstr ""

#: ../shortcuts/CompizShortcutModeller.cpp:395
msgid "Semi-maximise the current window."
msgstr ""

#: ../plugins/unityshell/unityshell.xml.in.h:38
msgid "Shadow X offset"
msgstr ""

#: ../plugins/unityshell/unityshell.xml.in.h:40
msgid "Shadow Y offset"
msgstr ""

#: ../launcher/DesktopLauncherIcon.cpp:57
#: ../plugins/unityshell/unityshell.xml.in.h:12
msgid "Show Desktop"
msgstr ""

#: ../plugins/unity-mt-grab-handles/unitymtgrabhandles.xml.in.h:5
msgid "Show Handles"
msgstr ""

#: ../plugins/unityshell/unityshell.xml.in.h:142
msgid "Show live previews of windows in the Switcher"
msgstr ""

#: ../shutdown/SessionView.cpp:144 ../shutdown/SessionButton.cpp:75
msgid "Shut Down"
msgstr ""

#: ../plugins/unityshell/unityshell.xml.in.h:70
msgid "Slide only"
msgstr ""

#: ../plugins/unity-mt-grab-handles/unitymtgrabhandles.xml.in.h:2
msgid "Small touch-based grab handles to move and resize a window"
msgstr ""

#: ../dash/ScopeView.cpp:751
msgid "Sorry, there is nothing that matches your search."
msgstr ""

#: ../shortcuts/CompizShortcutModeller.cpp:353
msgid "Spreads all windows in all the workspaces."
msgstr ""

#: ../shortcuts/CompizShortcutModeller.cpp:344
msgid "Spreads all windows in the current workspace."
msgstr ""

#: ../shortcuts/CompizShortcutModeller.cpp:370
msgid "Spreads all windows of the focused application in all the workspaces."
msgstr ""

#: ../shortcuts/CompizShortcutModeller.cpp:361
msgid ""
"Spreads all windows of the focused application in the current workspace."
msgstr ""

#: ../shortcuts/CompizShortcutModeller.cpp:362
msgid "Spreads all windows of the focused application."
msgstr ""

#: ../shortcuts/CompizShortcutModeller.cpp:345
msgid "Spreads all windows."
msgstr ""

#: ../plugins/unityshell/unityshell.xml.in.h:29
msgid "Static Blur"
msgstr ""

#: ../plugins/networkarearegion/networkarearegion.xml.in.h:2
msgid "Support _UNITY_NET_WORKAREA_REGION"
msgstr ""

#: ../shutdown/SessionButton.cpp:67
msgid "Suspend"
msgstr ""

#: ../lockscreen/UserPromptView.cpp:509
msgid "Switch to greeter…"
msgstr ""

#: ../a11y/unity-switcher-accessible.cpp:147
#: ../plugins/unityshell/unityshell.xml.in.h:127
msgid "Switcher"
msgstr ""

#: ../plugins/unityshell/unityshell.xml.in.h:140
msgid "Switcher strictly changes between applications"
msgstr ""

#: ../shortcuts/CompizShortcutModeller.cpp:141
msgid "Switches applications via the Launcher."
msgstr ""

#: ../shortcuts/CompizShortcutModeller.cpp:206
msgid "Switches between Lenses."
msgstr ""

#: ../shortcuts/CompizShortcutModeller.cpp:273
msgid "Switches between applications from all workspaces."
msgstr ""

#: ../shortcuts/CompizShortcutModeller.cpp:264
msgid "Switches between applications."
msgstr ""

#: ../shortcuts/CompizShortcutModeller.cpp:320
msgid "Switches between workspaces."
msgstr ""

#: ../plugins/unityshell/unityshell.xml.in.h:129
msgid "Switches to the next open window on the actual viewport."
msgstr ""

#: ../plugins/unityshell/unityshell.xml.in.h:133
msgid "Switches to the next open window, including windows of all viewports."
msgstr ""

#: ../plugins/unityshell/unityshell.xml.in.h:131
msgid ""
"Switches to the previous open window on the actual viewport, once the "
"Switcher has been revealed."
msgstr ""

#: ../plugins/unityshell/unityshell.xml.in.h:135
msgid ""
"Switches to the previous open window, once the Switcher has been revealed, "
"including windows of all viewports."
msgstr ""

#: ../shortcuts/CompizShortcutModeller.cpp:280
msgid "Switches windows of current applications."
msgstr ""

#: ../shortcuts/CompizShortcutModeller.cpp:326
msgid "Switches workspaces."
msgstr ""

#: ../shortcuts/CompizShortcutModeller.cpp:261
msgid "Switching"
msgstr ""

#: ../shortcuts/CompizShortcutModeller.cpp:254
msgid "Take a screenshot of the current window."
msgstr ""

#: ../shortcuts/CompizShortcutModeller.cpp:249
msgid "Take a screenshot."
msgstr ""

#: ../plugins/unityshell/unityshell.xml.in.h:45
msgid "The color of the shadows for the active window."
msgstr ""

#: ../plugins/unityshell/unityshell.xml.in.h:49
msgid "The color of the shadows for the inactive windows."
msgstr ""

#: ../launcher/DeviceNotificationDisplayImp.cpp:41
msgid "The drive has been successfully ejected"
msgstr ""

#: ../plugins/unityshell/unityshell.xml.in.h:108
msgid "The icon animation playing during the launch of a process."
msgstr ""

#: ../plugins/unityshell/unityshell.xml.in.h:113
msgid "The icon animation playing when a Launcher Icon is in the urgent state."
msgstr ""

#: ../plugins/unityshell/unityshell.xml.in.h:95
msgid "The maximum velocity at which the mouse will still be stopped."
msgstr ""

#: ../plugins/unityshell/unityshell.xml.in.h:32
msgid "The minimum value to trigger automaximize."
msgstr ""

#: ../plugins/unityshell/unityshell.xml.in.h:97
msgid ""
"The number of milliseconds Sticky Edges are deactivated for after the "
"barrier has been broken."
msgstr ""

#: ../plugins/unityshell/unityshell.xml.in.h:62
msgid "The opacity of the Launcher background."
msgstr ""

#: ../plugins/unityshell/unityshell.xml.in.h:21
msgid "The opacity of the Panel background."
msgstr ""

#: ../plugins/unityshell/unityshell.xml.in.h:43
msgid "The radius of the shadow blur for the active window."
msgstr ""

#: ../plugins/unityshell/unityshell.xml.in.h:47
msgid "The radius of the shadow blur for the inactive windows."
msgstr ""

#: ../plugins/unityshell/unityshell.xml.in.h:93
msgid "The rate at which mouse pressure decays."
msgstr ""

#: ../plugins/unityshell/unityshell.xml.in.h:99
msgid "The size of the icons in the Launcher."
msgstr ""

#: ../decorations/DecorationsForceQuitDialog.cpp:328
msgid "This window is not responding"
msgstr ""

#: ../plugins/unityshell/unityshell.xml.in.h:144
msgid "Timed automatic to show live previews in the Switcher"
msgstr ""

#: ../plugins/unity-mt-grab-handles/unitymtgrabhandles.xml.in.h:3
msgid "Toggle Handles"
msgstr ""

#: ../plugins/unityshell/unityshell.xml.in.h:75
msgid "Top Left Corner"
msgstr ""

#: ../launcher/TrashLauncherIcon.cpp:51
msgid "Trash"
msgstr ""

#: ../plugins/unityshell/unityshell.xml.in.h:27
msgid "Type of blur in the Dash."
msgstr ""

#: ../hud/HudView.cpp:198 ../hud/HudView.cpp:385
msgid "Type your command"
msgstr ""

#: ../panel/PanelMenuView.cpp:80
msgid "Ubuntu Desktop"
msgstr ""

#. anonymous namespace
#. namespace unity
#: ../plugins/unityshell/unityshell.xml.in.h:1
msgid "Ubuntu Unity Plugin"
msgstr ""

#: ../plugins/unity-mt-grab-handles/unitymtgrabhandles.xml.in.h:1
msgid "Unity MT Grab Handles"
msgstr ""

#. namespace panel
#. namespace unity
#: ../plugins/networkarearegion/networkarearegion.xml.in.h:1
msgid "Unity Scrollbars Support"
msgstr ""

#: ../lockscreen/UserPromptView.cpp:469
msgid "Unlock"
msgstr ""

#: ../launcher/ApplicationLauncherIcon.cpp:590
#: ../launcher/ApplicationLauncherIcon.cpp:713
#: ../launcher/VolumeLauncherIcon.cpp:189
#, fuzzy
msgid "Unlock from Launcher"
msgstr "Launcher から削除"

#: ../launcher/VolumeLauncherIcon.cpp:355
msgid "Unmount"
msgstr ""

#: ../lockscreen/UserPromptView.cpp:101
msgid "Username"
msgstr ""

#: ../plugins/unityshell/unityshell.xml.in.h:41
msgid "Vertical offset of the shadow."
msgstr ""

#. Application is being installed, or hasn't been installed yet
#: ../launcher/SoftwareCenterLauncherIcon.cpp:75
msgid "Waiting to install"
msgstr ""

#: ../plugins/unityshell/unityshell.xml.in.h:23
msgid ""
"When a window is maximized and visible in the current viewport, the panel "
"opacity is disabled."
msgstr ""

#: ../plugins/unityshell/unityshell.xml.in.h:115
msgid "Wiggle"
msgstr ""

#: ../shortcuts/CompizShortcutModeller.cpp:340
msgid "Windows"
msgstr ""

#: ../launcher/ExpoLauncherIcon.cpp:118
#, c-format
msgid "Workspace %d"
msgstr ""

#: ../launcher/ExpoLauncherIcon.cpp:119
#, c-format
msgid "Workspace %dx%d"
msgstr ""

#: ../launcher/ExpoLauncherIcon.cpp:35
msgid "Workspace Switcher"
msgstr ""

#: ../shortcuts/CompizShortcutModeller.cpp:317
msgid "Workspaces"
msgstr ""

#: ../decorations/DecorationsForceQuitDialog.cpp:357
msgid "_Force Quit"
msgstr ""

#: ../decorations/DecorationsForceQuitDialog.cpp:350
msgid "_Wait"
msgstr ""

#, fuzzy
#~ msgid "Keep in launcher"
#~ msgstr "Launcher に残す"
